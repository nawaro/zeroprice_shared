import numpy as np
from bisect import bisect_left, bisect_right
from collections import Counter
from datetime import datetime

def binary_search(a, x, lo=0, hi=None):
    if hi is None:
        hi = len(a)
    pos = bisect_left(a, x, lo, hi)
    return pos # if pos != hi and a[pos] == x else -1


class Stepfun:
    def __init__(self, x, y, f='right', ties='ordered', right=False):
        if list(x) != sorted(list(x)):
            raise Exception('Stepfun: x must be ordered increasingly')
        n = len(x)
        if n < 1:
            raise Exception('x must have length >= 1')
        n1 = n + 1
        if len(y) != n1:
            raise Exception('y must be one longer than x')

        self.x = x
        self.y = y

    def get_size(self):
        return len(self.x)

    def get_knots(self):
        return self.x

    def get_pieces(self):
        return self.y

    def _get_value(self, value):
        index = binary_search(self.x, value)
        return self.y[index]

    def get_values(self, values):
        try:
            multiple_values = []
            for value in values:
                multiple_values.append(self._get_value(value))
            return np.array(multiple_values)
        except TypeError:  # not iterable
            return self._get_value(value)


class Ecdf(Stepfun):
    def __init__(self, values, counts):
        counts = np.array(counts)
        super().__init__(x=values, y=np.hstack([[0], np.cumsum(counts / counts.sum())]))


class Dgof:
    def __init__(self, values, counts_0, counts_1):
        self.x = values, counts_0
        self.y = Ecdf(values, counts_1)

        self._nm_alt = {"two_sided": "two-sided",
                        "less": "the CDF of x lies below that of y",
                        "greater": "the CDF of x lies above that of y"}

    @staticmethod
    def _alt_switch(dev, alternative):
        if alternative == 'two_sided':
            return np.max(np.abs(dev))
        elif alternative == 'greater':
            return np.max(dev)
        elif alternative == 'less':
            return np.max((-1)*dev)

    def _getks(self, a, knots_y, fknots_y, alternative):
        c = [(k, v) for k, v in Counter(a).items()]
        c.sort(key=lambda x: x[0])
        values, counts = [x[0] for x in c], [x[1] for x in c]
        aecdf = Ecdf(values, counts)
        dev = np.hstack([[0], aecdf.get_values(knots_y) - fknots_y])
        return self._alt_switch(dev, alternative)

    def _sim_pval(self, alternative, statistic, tol, b):
        knots_y = self.y.get_knots()
        fknots_y = self.y.get_values(knots_y)

        s_all = []
        for _ in range(b):
            u = np.random.uniform(size=int(sum(self.x[1])))
            pc = self.y.get_pieces()
            u = np.array([knots_y[a-1] for a in np.searchsorted(pc, u)])
            u.shape = (1, int(sum(self.x[1])))

            s = self._getks(u[0], knots_y, fknots_y, alternative)

            s_all.append(s)

        ## calculate b values of the test statistic
        return (sum(np.array(s_all) >= statistic - tol) / b)

    def kstest(self, alternative='two_sided', exact=None, tol=1e-8, simulate_p_value=False, b=200):
        # Kolmogorov-Smirnov test for step functions

        old = datetime.now()

        z = self.y.get_knots()

        if exact is None:
            exact = (self.y.get_size() <= 30)
        if (exact and self.y.get_size() > 30):
            warnings.warn('numerical instability may affect p-value')

        method = "One-sample Kolmogorov-Smirnov test"
        xecdf = Ecdf(self.x[0], self.x[1])
        dev = np.hstack([[0], np.hstack([xecdf.get_values(z), [1]]) - self.y.get_pieces()])
        statistic = self._alt_switch(dev, alternative)

        if simulate_p_value:
            pval = self._sim_pval(alternative, statistic, tol=tol, b=b)
        else:
            raise Exception('Not implemented yet')

        print(datetime.now() - old)

        return {
        'statistic': statistic,
        'p_value': pval,
        'alternative': self._nm_alt[alternative],
        'method': method,
        }
