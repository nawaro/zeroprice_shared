import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from ast import literal_eval
import os
from kstest_lowmemory import Dgof

import seaborn as sns
sns.set_style('whitegrid')
sns.set_context('poster', rc={"lines.linewidth": 2.5})

pars_dir = os.path.join('..', 'out', 'pars')

dfs = []
for filename in sorted(os.listdir(pars_dir)):
    if not filename.endswith('.csv'):
        continue
    dfs.append(pd.concat([pd.DataFrame({'identifier': [filename.replace('.csv', '')]}), pd.read_csv(os.path.join(pars_dir, filename), index_col=0)], axis=1))

res = pd.concat(dfs)

test_parts = ('left', 'right')
test_distrs = ('lognormalpositive', 'powerlaw', 'exponential', 'stretchedexponential')
test_tests = ('discreteks2s_stat', 'discreteks2s_pvalue', 'discreteks1s_stat', 'discreteks1s_pvalue', 'discretecvm_stat', 'discretecvm_pvalue')#, 'discretechisq_stat', 'discretechisq_pvalue')
test_cols = ['identifier']
for part in test_parts:
    for distr in test_distrs:
        for test in test_tests:
            test_cols.append('_'.join([part, 'fit', distr, test]))

for col in res.columns:
    if (col.endswith('_pvalue') or col.endswith('_stat')) and ('bestfit_' not in col):
        res[col] = res[col].astype('float64')

res.drop_duplicates(inplace=True)
print(res.shape)
parts = ['left', 'right']
tests_methods = [{'suffix': '_discreteks2s_stat', 'testname': 'discreteks2s', 'fun': min},
    {'suffix': '_discreteks1s_stat', 'testname': 'discreteks1s', 'fun': min},
    {'suffix': '_discretecvm_stat', 'testname': 'discretecvm', 'fun': min},
    {'suffix': '_aic', 'testname': 'aic', 'fun': min, 'significance_name': 'discreteks1s'},
    {'suffix': '_loglik', 'testname': 'loglik', 'fun': max, 'significance_name': 'discreteks1s'},
     ]

def safenan(row, key, return_if_nan=0, return_if_keyerror=0):
    try:
        x = row[key]
        if np.isnan(x):
            return return_if_nan
        else:
            return x
    except KeyError:
        return return_if_keyerror


data_final = []
for i, row in res.iterrows():
    data_res = row.to_dict()
    for tests_method in tests_methods:
        suffix, testname, fun = tests_method['suffix'], tests_method['testname'], tests_method['fun']
        bestfits = {}
            
        for part in parts:
            print(row)
            print(suffix, testname, part)
            aics = {k: v for k, v in row.to_dict().items() if
                    k.endswith(suffix) and k.startswith(part) and (not 'significance_' in k) and (not 'bestfit_' in k)}
            bestfitdistr_name, bestfitdistr_value = fun(aics.items(), key=lambda x: x[1])
            bestfitdistr_name = bestfitdistr_name.split('_')[2]
            bestfits[part] = bestfitdistr_name

            if data_res['seconds'] == 60 and tests_method == tests_methods[0]:
                for test_distr in test_distrs:
                    identifier = '{}_{}'.format(data_res['identifier'], part)
                    x = np.loadtxt('../out/cls/cl_{}.txt'.format(identifier))
                    y = np.loadtxt('../out/cls/clpred_{}_{}.txt'.format(identifier, test_distr))
                    a = np.loadtxt('../out/cls/xs_{}.txt'.format(identifier))
                    apred = np.loadtxt('../out/cls/xspred_{}.txt'.format(identifier))
                    dgof = Dgof(a, x, y[np.where(apred == a[0])[0][0] : (np.where(apred == a[-1])[0][0] + 1)])

                    pythontest_result = dgof.kstest(simulate_p_value=True)
                    data_res['{}_fit_{}_pythonks_pvalue'.format(part, test_distr)] = pythontest_result['p_value']
                    data_res['{}_fit_{}_pythonks_stat'.format(part, test_distr)] = pythontest_result['statistic']


            data_res['bestfit_{}_{}'.format(testname, part)] = bestfitdistr_name

            if tests_method.get('significance_name') is None:
                data_res['significance_{}_{}'.format(testname, part)] = safenan(row, '{}_fit_{}_{}_pvalue'.format(
                        part, bestfitdistr_name, testname))
            else:
                data_res['significance_{}_{}'.format(testname, part)] = safenan(row, '{}_fit_{}_{}_pvalue'.format(
                        part, bestfitdistr_name, tests_method['significance_name']))


        data_res['border_bestfit_{}_left'.format(testname)] = data_res['left_fit_{}_border'.format(bestfits['left'])]
        data_res['border_bestfit_{}_right'.format(testname)] = data_res['right_fit_{}_border'.format(bestfits['right'])]
        data_res['zeroeffect_{}'.format(testname)] = (
            float(data_res['left_fit_{}_border'.format(bestfits['left'])]) - float(data_res['right_fit_{}_border'.format(bestfits['right'])]
            )) / float(data_res['left_fit_{}_border'.format(bestfits['left'])])


    data_final.append(data_res)


pd.DataFrame(data_final).to_csv('../out/finalchart_discreter.csv')
