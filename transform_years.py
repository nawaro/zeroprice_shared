import calendar

import pandas as pd
import numpy as np
import os
from datetime import datetime
import gc
import pyproj
from bisect import bisect_left


class City:
    def __init__(self, data, city, time_suffix,
                 start_date_col, end_date_col,
                 group_col, duration_col,
                 start_station_col, end_station_col, bike_col,
                 user_col=None, search_divisor=30,
                 do_not_remove=tuple(),
                 ):
        self.df = data
        for col in self.df.columns:
            if col in [start_date_col, end_date_col,
                       group_col, duration_col, start_station_col, end_station_col, bike_col, user_col,
                       'start_lat', 'start_lon', 'end_lat', 'end_lon'] + list(do_not_remove):
                continue
            print(col, 'delete')
            del self.df[col]
            gc.collect()
        self.df['index_class'] = range(df.shape[0])
        print(self.df.shape)
        self.city = city
        self.time_suffix = time_suffix

        self.start_date_col = start_date_col
        self.end_date_col = end_date_col
        self.group_col = group_col
        self.duration_col = duration_col
        self.start_station_col = start_station_col
        self.end_station_col = end_station_col
        self.bike_col = bike_col

        self.end_dates = {}

        self.user_col = user_col
        self.search_divisor = search_divisor

    def groups(self, continued_col, suffix=''):
        if self.group_col is None:
            self.group_col = 'group_col'
            self.df[self.group_col] = 'all'
        df_continued = self.df.loc[self.df[continued_col] == False]
        for ctype, cdf in df_continued.groupby(self.group_col):
            print(ctype, cdf.shape)
            counts = cdf[self.duration_col].value_counts()
            counts_df = pd.DataFrame(counts).reset_index()
            counts_df.columns = ['duration', 'count']
            counts_df.sort_values('duration', inplace=True)
            counts_df.reset_index(drop=True).to_pickle(
                '../out/duration{}_{}_{}{}.pickle'.format(
                    self.time_suffix, self.city, ctype, suffix))

    def get_df_any_columns(self):
        self.df['any_False'] = False
        cols = [x for x in self.df.columns if x.startswith('any_')]
        print(cols)
        return cols

    def prohibited_dates(self, delta_maximums=(2, 5, 30)):
        self.df['start_date_minute_col'] = self.df[self.start_date_col].dt.floor('Min')
        self.df['end_date_minute_col'] = self.df[self.end_date_col].dt.floor('Min')
        for delta_maximum in delta_maximums:
            end_dates = self.df.groupby(self.end_station_col)['end_date_minute_col'].apply(list).to_dict()
            print('start prohibited_dates', datetime.now())
            for k, v in end_dates.items():
                new_end_dates = set()
                max_new_end_dates = None
                v = [int(x.timestamp()) for x in v]
                for v1 in sorted(set(v)):
                    if max_new_end_dates is None:
                        max_new_end_dates = v1
                    new_end_dates.add(v1)

                    if (current_delta := ((max_new_end_dates - v1) // 60)) >= 0:
                        start_delta = current_delta
                    else:
                        start_delta = 0

                    for delta in range(60*start_delta, 60*(delta_maximum+1), 60):
                        new_end_dates.add(v1 + delta)
                    max_new_end_dates = v1 + delta
                end_dates[k] = sorted([pd.Timestamp(x * (10**9)) for x in new_end_dates])
            print('end prohibited_dates', datetime.now())
            self.end_dates[delta_maximum] = end_dates

    @staticmethod
    def bearing(start, finish):
        geodesic = pyproj.Geod(ellps='WGS84')
        fwd_azimuth, back_azimuth, distance = geodesic.inv(start[1], start[0], finish[1], finish[0])
        return fwd_azimuth

    @staticmethod
    def find_elem_in_sorted_list(elem, sorted_list):
        # https://docs.python.org/3/library/bisect.html
        i = bisect_left(sorted_list, elem)
        if i != len(sorted_list) and sorted_list[i] == elem:
            return True
        return False

    def bearings_trips(self, lss, station_col=None, lat_col=None, lon_col=None):
        print('bearings_trips start', datetime.now())
        if lss is not None:
            self.df = self.df.merge(lss.rename(columns={lat_col: 'start_lat', lon_col: 'start_lon'}),
                                    left_on=self.start_station_col, right_on=station_col).merge(
                lss.rename(columns={lat_col: 'end_lat', lon_col: 'end_lon'}),
                left_on=self.end_station_col, right_on=station_col)
        print(self.df)
        self.df['bearing'] = self.df.apply(lambda row: self.bearing(
                [row['start_lat'], row['start_lon']], [row['end_lat'], row['end_lon']]), axis=1)
        print('bearings_trips done', datetime.now())

    def continued_trips(self, attempt_time=120, diff_times=(120, 600, 1800,), bearing_tols=(30, 45, 60,),
                        users_to_skip=tuple()):
        print('continued trips start', datetime.now())
        self.df = self.df.loc[~(
                (self.df[self.start_station_col] == self.df[self.end_station_col]
                 ) & (self.df[self.duration_col] <= attempt_time)
        )]

        if self.bike_col is not None:
            new_dfs = []

            for bid, dfid in self.df.groupby(self.bike_col):
                dfid = dfid.sort_values(self.start_date_col)
                dfid['shifted_end'] = dfid[self.end_date_col].shift(1)
                dfid['bearing_change'] = dfid['bearing'].diff(1) % 360
                dfid['diff'] = (dfid[self.start_date_col] - dfid['shifted_end']).apply(lambda x: x.total_seconds())

                for diff_time in diff_times:
                    dfid['continued'] = (dfid['diff'] <= diff_time)
                    dfid['next_continued'] = dfid['continued'].shift(-1).fillna(False)
                    dfid['any_bike_diff{}'.format(diff_time)] = dfid['continued'] | dfid['next_continued']
                    for bearing_tol in bearing_tols:
                        dfid['continued'] = (dfid['diff'] <= diff_time) & (
                                (dfid['bearing_change'] < bearing_tol) | (dfid['bearing_change'] > 360 - bearing_tol)
                        )
                        dfid['next_continued'] = dfid['continued'].shift(-1).fillna(False)
                        dfid['any_bike_diff{}_bearing{}'.format(
                            diff_time, bearing_tol)] = dfid['continued'] | dfid['next_continued']

                if self.end_dates:
                    for k, v in self.end_dates.items():
                        dfid['continued'] = dfid.apply(
                            lambda row: self.find_elem_in_sorted_list(
                                row['start_date_minute_col'], v.get(row[self.start_station_col], list())), axis=1)
                        dfid['next_continued'] = dfid['continued'].shift(-1).fillna(False)
                        dfid['any_bike_stations_times{}'.format(k)] = dfid['continued'] | dfid['next_continued']

                dfid = dfid.drop(columns=['continued', 'next_continued'])
                new_dfs.append(dfid)

            self.df = pd.concat(new_dfs)
        self.df['any_False'] = False

        print('continued trips bike col done, user col start', datetime.now())
        gc.collect()

        if self.user_col is None:
            return

        print(self.df[self.user_col])
        self.df = self.df.loc[~(self.df[self.user_col].isin(users_to_skip))]
        counter = 0
        new_dfs = []
        print(self.df[self.user_col].value_counts())
        for user_id, df_user in self.df.groupby(self.user_col):
            if user_id in users_to_skip:
                continue
            df_user = df_user.sort_values(self.start_date_col)

            df_user['i_user'] = list(range(df_user.shape[0]))
            df_user['previous_stations_user_return'] = df_user.apply(
                lambda row:
                df_user.iloc[max(0, row['i_user'] - (df_user.shape[0] // self.search_divisor)):row['i_user']].loc[
                    (
                            df_user[self.end_date_col] >= row[self.start_date_col] - pd.Timedelta(minutes=30)
                    ) & (
                            df_user[self.end_date_col] < row[self.start_date_col]
                    ) & (
                            df_user[self.end_station_col] == row[self.start_station_col]
                    )][[self.end_station_col, 'bearing', self.end_date_col, 'index_class']].to_dict(
                    orient='list'), axis=1)

            df_user['previous_reverse_index_return'] = df_user.apply(self.index_of_trip, axis=1)
            for col in ['bearing', 'index_class']:
                df_user['previous_{}'.format(col)] = df_user.apply(
                    lambda row: row['previous_stations_user_return'][col][::-1][int(idx)] if ~np.isnan(idx := row[
                        'previous_reverse_index_return']) else None, axis=1)
            df_user['bearing_diff'] = (df_user['bearing'] - df_user['previous_bearing'].fillna(0)) % 360

            df_user['continued'] = ~df_user['previous_reverse_index_return'].isnull()

            df_user['next_continued'] = False
            df_user.loc[df_user['index_class'].isin(
                df_user.loc[df_user['continued'] == True, 'previous_index_class']
            ), 'next_continued'] = True
            df_user['any_user_id'] = df_user['continued'] | df_user['next_continued']

            for bearing_tol in bearing_tols:
                df_user['continued'] = ~df_user['previous_bearing'].isnull() & (
                        (df_user['bearing_diff'] < bearing_tol) | (
                        df_user['bearing_diff'] > 360 - bearing_tol)
                    )
                df_user['next_continued'] = False
                df_user.loc[df_user['index_class'].isin(
                    df_user.loc[df_user['continued'] == True, 'previous_index_class']
                ), 'next_continued'] = True
                df_user['any_user_id_bearing{}'.format(
                    bearing_tol)] = df_user['continued'] | df_user['next_continued']

            df_user = df_user.drop(columns=['continued', 'next_continued'])
            # print(df_user[[x for x in df_user.columns if x.startswith('any_')]].sum(axis=0))
            new_dfs.append(df_user)
            if counter % 10 == 0:
                print(counter, len(self.df[self.user_col].unique()), datetime.now())
            counter += 1
        self.df = pd.concat(new_dfs)
        gc.collect()
        print('continued trips done', datetime.now())

    def index_of_trip(self, row):
        previous_returns = row['previous_stations_user_return']
        try:
            latest_return = previous_returns[self.end_station_col][::-1].index(row[self.start_station_col])
            # latest_bearing = previous_returns['bearing'][::-1][latest_return]
        except ValueError:
            return np.nan
        return latest_return

    def shorten_dates(self, date_format, begin_date, finish_date, convert_datetime=True):
        if convert_datetime:
            self.df[self.start_date_col] = pd.to_datetime(self.df[self.start_date_col], format=date_format)
            self.df[self.end_date_col] = pd.to_datetime(self.df[self.end_date_col], format=date_format)
        print('shorten dates before', self.city, self.df.shape)
        self.df = self.df.loc[
            (self.df[self.start_date_col] >= begin_date) & (self.df[self.start_date_col] < finish_date)]

        self.df.sort_values(self.start_date_col, inplace=True)
        # self.df = self.df.iloc[:10000]

        print('shorten dates after', self.city, self.df.shape)

    def create_duration(self):
        self.df[self.duration_col] = (self.df[self.end_date_col] - self.df[self.start_date_col]).apply(
            lambda x: x.total_seconds())

    def sort_values(self, column_name):
        self.df = self.df.sort_values(column_name)

    def save_trips(self):
        print(self.df[self.get_df_any_columns()].sum(axis='rows') / self.df.shape[0])
        gc.collect()
        self.df.to_csv('../out/final_{}_{}.csv'.format(self.time_suffix, self.city))

    def remove_df(self):
        del self.df
        gc.collect()


month_abbr = calendar.month_abbr
periods_csvs = {
    'Philadelphia': {
        **{'{}x{}'.format(year, str(month).zfill(2)): '../in/indego-trips-20{}-q{}.csv'.format(
                   year, ((month - 1) // 3) + 1) for month in range(1, 13) for year in range(19, 20)},
    },
    'Chicago': {
        **{'{}x{}'.format(year, str(month).zfill(2)): '../in/Divvy_Trips_20{}_Q{}.csv'.format(
                   year, ((month - 1) // 3) + 1) for month in range(1, 13) for year in range(17, 21)},
        **{'{}x{}'.format(year, str(month).zfill(2)): '../in/20{}{}-divvy-tripdata.csv'.format(
            year, str(month).zfill(2)) for month in range(1, 13) for year in range(20, 22)},
        '20x01': '../in/Divvy_Trips_2020_Q1.csv',
        '20x02': '../in/Divvy_Trips_2020_Q1.csv',
        '20x03': '../in/Divvy_Trips_2020_Q1.csv',
    },
    'NYC': {'{}x{}'.format(year, str(month).zfill(2)): '../in/20{}{}-citibike-tripdata.csv'.format(
        year, str(month).zfill(2)) for month in range(1, 13) for year in range(17, 22)
    },
    'DC': {
        **{'{}x{}'.format(year, str(month).zfill(2)): '../in/20{}Q{}-capitalbikeshare-tripdata.csv'.format(
            year, ((month - 1) // 3) + 1) for month in range(1, 13) for year in range(17, 18)},
        **{'{}x{}'.format(year, str(month).zfill(2)): '../in/20{}{}-capitalbikeshare-tripdata.csv'.format(
            year, str(month).zfill(2)) for month in range(1, 13) for year in range(18, 22)}
    },
    'Warsaw': {
        '19x11': '../in/201911_veturilo.csv'
    },
    'London': {
        **{'19x{}'.format(str(month).zfill(2)): [
            '../in/{}'.format(x) for x in os.listdir('../in') if
            'JourneyData' in x and (not x.endswith('.1')) and (
                    month_abbr[month-1] in x or month_abbr[month] in x or month_abbr[month+1] in x)]
        for month in range(2,12)},
        **{'20x{}'.format(str(month).zfill(2)): [
            '../in/{}'.format(x) for x in os.listdir('../in') if
            'JourneyData' in x and (not x.endswith('.1')) and (
                    month_abbr[month-1] in x or month_abbr[month] in x or month_abbr[month+1] in x)]
        for month in range(2,12)},
        '19x01': [
            '../in/{}'.format(x) for x in os.listdir('../in') if
            'JourneyData' in x and (not x.endswith('.1')) and (
                    '2018' in x or month_abbr[1] in x or month_abbr[2] in x)],
        '19x12': [
            '../in/{}'.format(x) for x in os.listdir('../in') if
            'JourneyData' in x and (not x.endswith('.1')) and (
                    month_abbr[11] in x or month_abbr[12] in x)],
        '20x01': [
            '../in/{}'.format(x) for x in os.listdir('../in') if
            'JourneyData' in x and (not x.endswith('.1')) and (
                    '2019' in x or month_abbr[1] in x or month_abbr[2] in x)],
        '20x12': [
            '../in/{}'.format(x) for x in os.listdir('../in') if
            'JourneyData' in x and (not x.endswith('.1')) and (
                    month_abbr[11] in x or month_abbr[12] in x)],
    }
}

dates_main = (
        ('19x07', '2019-07-01', '2019-08-01'),
        ('20x07', '2020-07-01', '2020-08-01'),
        ('19x11', '2019-11-01', '2019-12-01'),
        ('20x11', '2020-11-01', '2020-12-01'),
)

dates_all = []
for year in range(17, 22):
    for month in range(1, 13):
        if year == 21 and (month > 9):
            continue
        if month < 12:
            dates_all.append(('{}x{}'.format(year, str(month).zfill(2)),
                '20{}-{}-01'.format(year, str(month).zfill(2)),
                '20{}-{}-01'.format(year, str(month+1).zfill(2))
            ))
        else:
            dates_all.append(('{}x{}'.format(year, str(month).zfill(2)),
                '20{}-{}-01'.format(year, str(month).zfill(2)),
                '20{}-01-01'.format(year+1)
            ))


def read_dfs_names(city, period):
    if type(df_main := periods_csvs[city][period]) == str:
        dfs = {period: [df_main]}
    elif type(df_main) == list:
        dfs = {period: df_main}
    elif type(df_main) == dict:
        dfs = {'{}m{}'.format(period, k): [v] for k, v in df_main.items()}
    return dfs, df_main


def single_df(df_main, period_name, df_filenames, start_date_col):
    df = pd.concat([pd.read_csv(df_filename, low_memory=False) for df_filename in df_filenames])
    return df

cities = [
    {'city': 'Warsaw', 'compute_all': True, 'period': '19x11', 'start_date': '2019-11-01', 'end_date': '2019-12-01'},
] + [
    {'city': city, 'period': x[0], 'start_date': x[1], 'end_date': x[2]}
     for x in dates_all for city in ['Chicago', 'DC', 'NYC']
] + [
    {'city': city, 'compute_all': True, 'period': x[0], 'start_date': x[1], 'end_date': x[2]}
     for x in dates_main for city in ['Chicago', 'DC', 'NYC']
] + [
      {'city': city, 'compute_all': True, 'period': x[0], 'start_date': x[1], 'end_date': x[2]}
     for x in [i for i in dates_main] for city in ['London']
]

for city_settings in cities:
    city = city_settings['city']
    period = city_settings['period']
    start_date = pd.to_datetime(city_settings['start_date'])
    end_date = pd.to_datetime(city_settings['end_date'])
    print(period, start_date, end_date)
    df = pd.DataFrame()

    if city == 'test':
        # Chicago
        dfs_names, df_main = read_dfs_names('Chicago', period)

        for period_name, df_filenames in dfs_names.items():
            df = single_df(df_main, period_name, df_filenames, 'start_time')
            df['tripduration'] = df['tripduration'].fillna('').str.replace(',', '').astype(float).astype(int)
            df['user_id'] = df['usertype'] + '_' + df['birthyear'].fillna(0).astype(int).astype(str) + '_' + df['gender'].astype(str)
            df['from_station_id'] = df['from_station_id'].astype(str)
            df['to_station_id'] = df['to_station_id'].astype(str)
            test = City(data=df, city='test', time_suffix=period_name,
                start_date_col='start_time', end_date_col='end_time',
                group_col='usertype', duration_col='tripduration',
                start_station_col='from_station_id', end_station_col='to_station_id',
                bike_col='bikeid',
                user_col='user_id')

            test.shorten_dates(date_format='%Y-%m-%d %H:%M:%S',
                begin_date=start_date, finish_date=end_date)
            test.df = test.df.iloc[:25000]
            test.prohibited_dates(delta_maximums=(2,))
            cstest = pd.read_json('../in/divvy_chicago_stations.json')
            cstest['station'] = cstest['features'].apply(lambda x: x['properties'].get('station', {}).get('id', 'no id')).astype(str)
            cstest = cstest.loc[cstest['station'] != 'no id']
            cstest['lat'] = cstest['features'].apply(lambda x: x['geometry']['coordinates'][1])
            cstest['lon'] = cstest['features'].apply(lambda x: x['geometry']['coordinates'][0])
            cstest_sh = cstest[['station', 'lat', 'lon']]
            test.bearings_trips(lss=cstest_sh, station_col='station', lat_col='lat', lon_col='lon')
            test.continued_trips()
            test.save_trips()
            for col in test.get_df_any_columns():
                test.groups(continued_col=col, suffix='_' + col)
            test.remove_df()
            del test
            gc.collect()

    if city == 'Chicago':
        # Chicago
        dfs_names, df_main = read_dfs_names('Chicago', period)

        for period_name, df_filenames in dfs_names.items():
            df = single_df(df_main, period_name, df_filenames, 'start_time')
            df.rename(columns={'01 - Rental Details Rental ID': 'trip_id', '01 - Rental Details Local Start Time': 'start_time', '01 - Rental Details Local End Time': 'end_time', '01 - Rental Details Bike ID': 'bikeid', '01 - Rental Details Duration In Seconds Uncapped': 'tripduration', '03 - Rental Start Station ID': 'from_station_id', '03 - Rental Start Station Name': 'from_station_name', '02 - Rental End Station ID': 'to_station_id', '02 - Rental End Station Name': 'to_station_name', 'User Type': 'usertype', 'Member Gender': 'gender', '05 - Member Details Member Birthday Year': 'birthyear'}, inplace=True)
            print(df.head())
            print(df.columns)

            usertype_col = 'usertype'
            start_station_col = 'from_station_id'
            end_station_col = 'to_station_id'
            start_date_col = 'start_time'
            end_date_col = 'end_time'
            user_col = 'user_id'
            print(period_name[:2])
            if period_name[:2] not in ('17', '18', '19',):
                usertype_col = 'member_casual'
                start_station_col = 'start_station_id'
                end_station_col = 'end_station_id'
                start_date_col = 'started_at'
                end_date_col = 'ended_at'
                user_col = None
            print(usertype_col)

            if 'rideable_type' not in df.columns:
                df['rideable_type'] = 'docked_bike'

            print('all bikes', df.shape)
            df = df.loc[df['rideable_type'] != 'electric_bike']
            print('non-electric', df.shape)

            df[start_station_col] = df[start_station_col].astype(str)
            df[end_station_col] = df[end_station_col].astype(str)
            chicago = City(data=df, city='ChicagoClassic', time_suffix='{}'.format(period),
                start_date_col=start_date_col, end_date_col=end_date_col,
                group_col=usertype_col, duration_col='tripduration',
                start_station_col=start_station_col, end_station_col=end_station_col,
                bike_col='bikeid',
                user_col=user_col,
                do_not_remove=('birthyear', 'gender', 'start_lng', 'end_lng'))

            chicago.shorten_dates(date_format='%m/%d/%Y %H:%M' if period_name.startswith('17x1') else ('%m/%d/%Y %H:%M:%S'
                if period_name.startswith('17x0') else '%Y-%m-%d %H:%M:%S'),
                begin_date=start_date, finish_date=end_date)
            if 'tripduration' not in df.columns:
                chicago.create_duration()
            else:
                chicago.df['tripduration'] = chicago.df['tripduration'].astype(str).str.replace(',', '').astype(float).astype(int)
            
            lat_df = False
            if 'start_lat' not in chicago.df.columns:
                lat_df = True
                cs = pd.read_json('../in/divvy_chicago_stations.json')
                cs['station'] = cs['features'].apply(lambda x: x['properties'].get('station', {}).get('id', 'no id')).astype(str)
                cs = cs.loc[cs['station'] != 'no id']
                cs['lat'] = cs['features'].apply(lambda x: x['geometry']['coordinates'][1])
                cs['lon'] = cs['features'].apply(lambda x: x['geometry']['coordinates'][0])
                cs_sh = cs[['station', 'lat', 'lon']]
            else:
                chicago.df.rename(columns={'start_lng': 'start_lon', 'end_lng': 'end_lon'},
                    inplace=True)
                print(chicago.df.columns.tolist())

            if city_settings.get('compute_all'):
                if user_col == 'user_id':
                    chicago.df['user_id'] = chicago.df[usertype_col] + '_' + chicago.df['birthyear'].fillna(0).astype(int).astype(str) + '_' + chicago.df[
                        'gender'].astype(str)

                chicago.prohibited_dates()
                chicago.bearings_trips(lss=cs_sh if lat_df else None, station_col='station', lat_col='lat', lon_col='lon')
                chicago.continued_trips(users_to_skip=('Customer_0_nan',))

            chicago.save_trips()
            for col in chicago.get_df_any_columns():
                chicago.groups(continued_col=col, suffix='_' + col)
            chicago.remove_df()
            del chicago
            gc.collect()

    if city == 'London':
        # London
        dfs_names, df_main = read_dfs_names('London', period)

        for period_name, df_filenames in dfs_names.items():
            df = single_df(df_main, period_name, df_filenames, 'start_time')

            london = City(data=df, city='London', time_suffix='{}'.format(period_name),
                          start_date_col='Start Date', end_date_col='End Date',
                          group_col=None, duration_col='Duration',
                          start_station_col='StartStation Id', end_station_col='EndStation Id',
                          bike_col='Bike Id'
                          )

            london.shorten_dates(date_format='%d/%m/%Y %H:%M',
                                 begin_date=start_date, finish_date=end_date)

            ls = pd.read_json('../in/london_stations.json')
            ls['station'] = ls['url'].str.split('_').apply(lambda x: int(x[1]))
            ls_sh = ls[['station', 'lat', 'lon']]

            if city_settings.get('compute_all'):
                london.prohibited_dates()
                london.bearings_trips(lss=ls_sh, station_col='station', lat_col='lat', lon_col='lon')
                london.continued_trips()
            london.save_trips()

            for col in london.get_df_any_columns():
                london.groups(continued_col=col, suffix='_' + col)
            london.remove_df()
            del london
            gc.collect()

    if city == 'Philadelphia':
        # Philadelphia
        dfs_names, df_main = read_dfs_names('Philadelphia', period)

        for period_name, df_filenames in dfs_names.items():
            df = single_df(df_main, period_name, df_filenames, 'start_time')

            philly = City(data=df, city='Philadelphia', time_suffix=period_name,
                          start_date_col='start_time', end_date_col='end_time',
                          group_col='passholder_type', duration_col='duration_sec',
                          start_station_col='start_station', end_station_col='end_station',
                          bike_col='bike_id',
                          )

            format_philly = '%m/%d/%Y %H:%M' if period_name == '19x07' else '%Y-%m-%d %H:%M:%S'
            philly.shorten_dates(date_format=format_philly,
                                 begin_date=start_date, finish_date=end_date)
            philly.create_duration()

            if city_settings.get('compute_all'):
                philly.prohibited_dates()
                philly.bearings_trips(None)
                philly.continued_trips()
            philly.save_trips()

            for col in philly.get_df_any_columns():
                philly.groups(continued_col=col, suffix='_' + col)
            philly.remove_df()
            del philly
            gc.collect()

    if city == 'Warsaw':
        # Warsaw
        dfs_names, df_main = read_dfs_names('Warsaw', period)

        for period_name, df_filenames in dfs_names.items():
            df = single_df(df_main, period_name, df_filenames, 'start_time')
            print(df['start_time'].sort_values())
            df['start_lat'] = df['start_lat'].astype(float)
            df['start_lon'] = df['start_lon'].astype(float)
            df['end_lat'] = df['end_lat'].astype(float)
            df['end_lon'] = df['end_lon'].astype(float)
            df['start_time'] = df['start_time'].apply(pd.to_datetime)
            df['end_time'] = df['end_time'].apply(pd.to_datetime)

            df['duration'] *= 60
            warsaw = City(data=df, city='Warsaw', time_suffix=period,
                          start_date_col='start_time', end_date_col='end_time',
                          group_col=None, duration_col='duration',
                          start_station_col='start_place_name', end_station_col='end_place_name',
                          bike_col='start_number',
                          )

            warsaw.shorten_dates(date_format='%Y-%m-%d %H:%M:%S', convert_datetime=False,
                                 begin_date=start_date, finish_date=end_date)

            if city_settings.get('compute_all'):
                warsaw.prohibited_dates()
                warsaw.bearings_trips(lss=None)
                warsaw.continued_trips()
            warsaw.save_trips()

            for col in warsaw.get_df_any_columns():
                warsaw.groups(continued_col=col, suffix='_' + col)
            warsaw.remove_df()
            del warsaw
            gc.collect()

    if city == 'NYC':
        # NYC
        dfs_names, df_main = read_dfs_names('NYC', period)

        new_periods = ['21x{}'.format(str(month).zfill(2)) for month in range(2, 13)] 
        old_periods = ['']
        for period_name, df_filenames in dfs_names.items():
            df = single_df(df_main, period_name, df_filenames, 'starttime')

            if period_name not in new_periods:
                df.rename(columns={
                    'start station latitude': 'start_lat',
                    'end station latitude': 'end_lat',
                    'start station longitude': 'start_lon',
                    'end station longitude': 'end_lon',
                    'Trip Duration': 'tripduration', 'Start Time': 'starttime', 'Stop Time': 'stoptime', 'Start Station ID': 'start station id', 'Start Station Name': 'start station name', 'Start Station Latitude': 'start station latitude', 'Start Station Longitude': 'start station longitude', 'End Station ID': 'end station id', 'End Station Name': 'end station name', 'End Station Latitude': 'end station latitude', 'End Station Longitude': 'end station longitude', 'Bike ID': 'bikeid', 'User Type': 'usertype', 'Birth Year': 'birth year', 'Gender': 'gender'
                }, inplace=True)
                df['user_id'] = df['usertype'] + '_' + df['birth year'].astype(str) + '_' + df['gender'].astype(str)
            else:
                df.rename(columns={
                    'start_lng': 'start_lon',
                    'end_lng': 'end_lon',
                }, inplace=True)

            # if (period_name in new_periods):
                # df['tripduration'] = (df['ended_at'].apply(pd.to_datetime, format='%Y-%m-%d %H:%M:%S') - df['started_at'].apply(pd.to_datetime, format='%Y-%m-%d %H:%M:%S')).apply(lambda x: x. total_seconds())

            nyc = City(data=df, city='{}{}'.format('NYC', period_name), time_suffix='{}'.format(period_name),
                       start_date_col='starttime' if period_name not in new_periods else 'started_at',
                       end_date_col='stoptime' if period_name not in new_periods else 'ended_at',
                       group_col='usertype' if period_name not in new_periods else 'member_casual',
                       duration_col='tripduration',
                       start_station_col='start station id' if period_name not in new_periods else 'start_station_id',
                       end_station_col='end station id' if period_name not in new_periods else 'end_station_id',
                       bike_col='bikeid' if period_name not in new_periods else None,
                       user_col='user_id' if period_name not in new_periods else None)
            nyc.shorten_dates(date_format='%Y-%m-%d %H:%M:%S', begin_date=start_date, finish_date=end_date)
            if period_name in new_periods:
                nyc.create_duration()

            if city_settings.get('compute_all'):
                nyc.prohibited_dates()
                nyc.bearings_trips(None)
                nyc.continued_trips(users_to_skip=('Customer_1969_0', 'Customer_nan_0'))
            nyc.save_trips()

            print(nyc.df.head())
            for col in nyc.get_df_any_columns():
                nyc.groups(continued_col=col, suffix='_' + col)

            nyc.remove_df()
            del nyc
            gc.collect()

    if city == 'DC':
        # Washington
        dfs_names, df_main = read_dfs_names('DC', period)

        new_periods = ['20x{}'.format(str(month).zfill(2)) for month in range (4, 13)] + ['21x{}'.format(str(month).zfill(2)) for month in range (1, 13)] 
        for period_name, df_filenames in dfs_names.items():
            df = single_df(df_main, period_name, df_filenames, 'Start date')

            if period_name in new_periods:
                df.rename(columns={'started_at': 'Start date', 'ended_at': 'End date', 'member_casual': 'Member type',
                                   'tripduration': 'Duration',
                                   'start_station_name': 'Start station', 'end_station_name': 'End station'},
                          inplace=True)

            if 'rideable_type' not in df.columns:
                df['rideable_type'] = 'docked_bike'

            print('all bikes', df.shape)
            df = df.loc[df['rideable_type'] != 'electric_bike']
            print('non-electric', df.shape)

            washington = City(data=df, city='WashingtonClassic', time_suffix='{}'.format(period_name),
                              start_date_col='Start date', end_date_col='End date',
                              group_col='Member type', duration_col='Duration',
                              start_station_col='Start station', end_station_col='End station',
                              bike_col='Bike number' if period_name not in new_periods else None
                              )

            washington.shorten_dates('%Y-%m-%d %H:%M:%S', begin_date=start_date, finish_date=end_date)

            if period_name in new_periods:
                washington.create_duration()

            cabis = pd.read_json('../in/cabi_washington_stations.json')
            ws = pd.DataFrame(cabis.iloc[0]['data']['stations'])
            ws['lat'] = ws['location'].apply(lambda x: x['lat'])
            ws['lon'] = ws['location'].apply(lambda x: x['lng'])
            ws.rename(columns={'stationName': 'station'}, inplace=True)
            ws_sh = ws[['station', 'lat', 'lon']]

            if city_settings.get('compute_all'):
                washington.prohibited_dates()
                washington.bearings_trips(lss=ws_sh, station_col='station', lat_col='lat', lon_col='lon')
                washington.continued_trips()

            washington.save_trips()
            for col in washington.get_df_any_columns():
                washington.groups(continued_col=col, suffix='_' + col)
            washington.remove_df()
            del washington
            gc.collect()
