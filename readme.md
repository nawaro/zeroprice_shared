## Zero-price effects in transportation: evidence from shared bicycles

### Data source

* Chicago: https://divvy-tripdata.s3.amazonaws.com/index.html
* London: https://cycling.data.tfl.gov.uk/ (JourneyDataExtract)
* New York: https://s3.amazonaws.com/tripdata/ (e.g. https://s3.amazonaws.com/tripdata/202109-citibike-tripdata.csv.zip)
* Washington: https://s3.amazonaws.com/capitalbikeshare-data/ (e.g. https://s3.amazonaws.com/capitalbikeshare-data/202109-capitalbikeshare-tripdata.zip)

### Files

* transform_years.py transform source .csv files to the required format
* charts_predict.py estimates the distributions
* ksprod.R runs the initial tests for comparability with other papers
* charts_order.py runs all the necessary tests
* charts_fromfinal.py creates charts.
