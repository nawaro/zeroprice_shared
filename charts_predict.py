import copy

import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import seaborn as sns
import powerlaw
from datetime import datetime
from scipy.stats import kstest, chisquare
from datetime import datetime
from collections import Counter
from itertools import product


default_min_time = 10
default_max_time = 75
default_seconds = 1
default_tol = 3
default_recompute = True
placebo_values = [0]
move_values = [0, 15]

class City:
    def __init__(self, city, split, border, single_any, month, filenames, shorten=False,
                 min_time=default_min_time, max_time=default_max_time, tol=default_tol,
                 seconds=default_seconds, recompute=default_recompute):
        self.seconds_minute = 60
        self.city = city
        self.split = split
        self.border = border
        self.min_time = min_time
        self.max_time = max_time
        self.tol = tol
        self.single_any = single_any
        self.seconds = seconds
        self.month = month
        self.filenames = filenames

        self.shorten = shorten

        self.final_identifier = '_'.join([self.month, self.city, self.split, self.single_any,
                                          str(self.tol), str(self.seconds)])
        if self.shorten:
            self.final_identifier = '_'.join([self.month, self.city, self.split, self.single_any,
                                              str(self.tol), str(self.seconds), 's' + str(self.shorten)])
        self.final_csv_name = '../{}/{}/{}.csv'.format(out_dir, 'pars_sh' if self.shorten else '', self.final_identifier)
        print(self.final_csv_name)

        self.df = None
        self.dr = None
        self.drs = {}
        self.xs = None
        self.modf = lambda x: x
        self.step = 1
        self.counts = {}
        self.fit_part = {}
        self.fit_distrs = {}

        self.predicteds_range = {}

        self.recompute = recompute

        self.x_col = 'duration'  # _{}s'.format(seconds)
        self.y_col = 'count'

        self.data_res = {'city': self.city, 'split': self.split, 'border': self.border,
                         'min_time': self.min_time, 'max_time': self.max_time, 'tol': self.tol, 'shorten': self.shorten,
                         'single_any': self.single_any,
                         'seconds': self.seconds, 'month': self.month}

    def check_if_file_exists(self):
        if os.path.exists(self.final_csv_name) and (not self.recompute):
            return True
        return False

    def generic_f(self, x, part, distr_name):
        return self.step * self.drs[part][self.y_col].sum() * functions_values(self.fit_part[part], distr_name, x)

    def load_files(self):
        dfs = []
        for filename in self.filenames:
            dfs.append(pd.read_pickle(filename).set_index('duration'))
        df = pd.concat(dfs, axis=1).sum(axis=1).reset_index()
        df.rename(columns={0: 'count'}, inplace=True)
        df['count'] = df['count'].astype(int)
        self.df = df

    def dr_transform(self):
        self.drs = {
                'left': self.dr.loc[(self.dr['duration_minute'] < (self.border - self.tol - self.shorten)) & (
                    self.dr['duration_minute'] >= self.min_time)],
                'right': self.dr.loc[(self.dr['duration_minute'] > (self.border + self.tol + self.shorten)) & (
                    self.dr['duration_minute'] < (self.max_time - self.tol))],
                'border': self.dr.loc[(self.dr['duration_minute'] >= (self.border - self.tol - self.shorten)) & (
                    self.dr['duration_minute'] <= (self.border + self.tol + self.shorten))],
                'total': self.dr.loc[(self.dr['duration_minute'] >= self.min_time) & (
                    self.dr['duration_minute'] <= self.max_time)]
            }

    def create_xs(self):
        self.xs = {
            'left': np.arange(self.modf(self.min_time) * (self.seconds_minute / self.seconds),
                              self.modf(self.border) * (self.seconds_minute / self.seconds) + 1, self.step),
            'right': np.arange(self.modf(self.border) * (self.seconds_minute / self.seconds),
                               self.modf(self.max_time) * (self.seconds_minute / self.seconds), self.step),
            'total': np.arange(self.modf(self.min_time) * (self.seconds_minute / self.seconds),
                               self.modf(self.max_time) * (self.seconds_minute / self.seconds), self.step),
        }

    def seconds_transform(self):
        self.df['duration_{}s'.format(self.seconds)] = (self.df['duration'] // self.seconds)
        self.dr = pd.DataFrame(self.df.groupby('duration_{}s'.format(self.seconds))['count'].sum())
        self.dr.reset_index(inplace=True)

        dr_final = pd.DataFrame({'duration_{}s'.format(self.seconds): np.arange(self.modf(self.min_time*(self.seconds_minute / self.seconds)), self.modf(self.max_time*(self.seconds_minute / self.seconds)))})
        self.dr = dr_final.merge(self.dr, on='duration_{}s'.format(self.seconds), how='left')
        self.dr.fillna(0, inplace=True)

        self.dr['duration'] = self.dr['duration_{}s'.format(self.seconds)]
        self.dr['duration_minute'] = self.dr['duration_{}s'.format(self.seconds)] * (self.seconds / self.seconds_minute)

        del self.dr['duration_{}s'.format(self.seconds)]

    def count(self):
        self.data_res['count_all'] = self.dr['count'].sum()
        self.counts['all'] = np.repeat(self.dr[self.x_col].tolist(), self.dr[self.y_col].tolist())
        for part, dr_part in self.drs.items():
            self.data_res['count_{}'.format(part)] = dr_part['count'].sum()
            self.counts[part] = np.repeat(dr_part[self.x_col].tolist(), dr_part[self.y_col].tolist())

    def fit_single_part(self, part):
        # logprint('fit', part)
        self.fit_part[part] = powerlaw.Fit(self.counts[part].tolist(), discrete=True,
                                xmin=self.drs[part][self.x_col].min(), xmax=self.drs[part][self.x_col].max(),
                                discrete_approximation='xmax')
        self.fit_distrs = distrs(self.fit_part[part])

        for fit_distr_name, fit_distr in self.fit_distrs.items():
            loglik = fit_distr.loglikelihood
            self.data_res['{}_fit_{}_loglik'.format(part, fit_distr_name)] = loglik
            self.data_res['{}_fit_{}_aic'.format(part, fit_distr_name)] = 2 * parameter_count[fit_distr_name] - 2 * loglik
            self.data_res['{}_fit_{}_par1'.format(part, fit_distr_name)] = fit_distr.parameter1
            self.data_res['{}_fit_{}_par2'.format(part, fit_distr_name)] = fit_distr.parameter2
            self.data_res['{}_fit_{}_par3'.format(part, fit_distr_name)] = fit_distr.parameter3
            self.data_res['{}_fit_{}_border'.format(part, fit_distr_name)] = self.generic_f(
                self.border * (self.seconds_minute / self.seconds), part, fit_distr_name)
            # print(fit_distr_name, fit_distr.parameter1, fit_distr.parameter2, self.data_res['{}_fit_{}_border'.format(part, fit_distr_name)])

        distrs_keys = sorted(list(self.fit_distrs.keys()))
        for i_distr_name, fit_distr_name0 in enumerate(distrs_keys):
            for fit_distr_name1 in distrs_keys[i_distr_name + 1:]:
                part_dc = self.fit_part[part].distribution_compare(
                    distrs_underscore[fit_distr_name0], distrs_underscore[fit_distr_name1])
                self.data_res[
                    '{}_{}_{}_loglikratio'.format(part, fit_distr_name0, fit_distr_name1)], self.data_res[
                    '{}_{}_{}_pvalue'.format(part, fit_distr_name0, fit_distr_name1)] = part_dc

    def predict(self, part):
        for fit_distr_name, fit_distr in self.fit_distrs.items():
            predicteds = np.array(
                [self.generic_f(x, part, fit_distr_name) for x in self.xs[part]])
            np.savetxt('../{}/{}/clpred_{}_{}_{}.txt'.format(out_dir, 'cls_sh' if self.shorten else 'cls', self.final_identifier, part, fit_distr_name), intize(predicteds))
        np.savetxt('../{}/{}/xspred_{}_{}.txt'.format(out_dir, 'cls_sh' if self.shorten else 'cls', self.final_identifier, part), self.xs[part].tolist())

    def save_main(self, part):
        np.savetxt('../{}/{}/cl_{}_{}.txt'.format(out_dir, 'cls_sh' if self.shorten else 'cls', self.final_identifier, part), self.drs[part][self.y_col].tolist())
        np.savetxt('../{}/{}/xs_{}_{}.txt'.format(out_dir, 'cls_sh' if self.shorten else 'cls', self.final_identifier, part), self.drs[part][self.x_col].tolist())

    def save_data_res(self):
        pd.DataFrame([self.data_res]).to_csv(self.final_csv_name)


for move_value, placebo_value in product(move_values, placebo_values):
    main_dir = '../out/'
    out_dir = 'out_m{}p{}'.format(move_value, placebo_value)

    for res_dir_name in ['cls', 'cls_sh', 'pars', 'pars_sh']:
        os.makedirs('../{}/{}'.format(out_dir, res_dir_name), exist_ok=True)

    test_run = False

    borders = {
        'NYC': {'Customer': 30, 'casual': 30,
                'Subscriber': 45, 'member': 45},
        'Warsaw': {'all': 20},
        'Chicago': {'Customer': 30, 'casual': 30,
                    'Subscriber': {**{'{}x{}'.format(year, str(month).zfill(2)): 30 for year in [17] for month in range(1,13)}, **{'{}x{}'.format(year, str(month).zfill(2)): 45 for year in range(18, 22) for month in range(1,13)}, '18x01': 30, '19full': 45, '20full': 45}, 'member': 45},
        'London': {'all': 30},
        'Philadelphia': {'Indego30': 60, 'Indego365': 60, 'Day Pass': 30},
        'Washington': {'Member': 30, 'Casual': 30, 'member': 30, 'casual': 30},
    }
    borders['ChicagoClassic'] = copy.deepcopy(borders['Chicago'])
    borders['WashingtonClassic'] = copy.deepcopy(borders['Washington'])

    splits = {
        'NYC': {'member': ['Subscriber', 'member'], 'casual': ['Customer', 'casual']},
        'Chicago': {'member': ['Subscriber', 'member'], 'casual': ['Customer', 'casual']},
        'Washington': {'member': ['Member', 'member'], 'casual': ['Casual', 'casual']},
    }
    splits['ChicagoClassic'] = copy.deepcopy(splits['Chicago'])
    splits['WashingtonClassic'] = copy.deepcopy(splits['Washington'])

    max_times = {
        'NYC': {'Customer': 45, 'casual': 45},
        'Washington': {'member': 60, 'Member': 60, 'Casual': 60, 'casual': 60},
        'Warsaw': {'all': 60},
        'Philadelphia': {'Indego30': 120, 'Indego365': 120},
        'Chicago': {'Customer': 60, 'casual': 60,
                    'Subscriber': {**{'{}x{}'.format(year, str(month).zfill(2)): 60 for year in [17] for month in range(1,13)}, **{'{}x{}'.format(year, str(month).zfill(2)): 75 for year in range(18, 22) for month in range(1,13)}, '18x01': 60, '19full': 75, '20full': 75}, 'member': 75},
    }
    max_times['ChicagoClassic'] = copy.deepcopy(max_times['Chicago'])
    max_times['WashingtonClassic'] = copy.deepcopy(max_times['Washington'])

    max_times = copy.deepcopy(borders)
    for k, v in borders.items():
        for k1, v1 in v.items():
            print(k1, type(v1))
            if isinstance(v1, dict):
                for k2, v2 in v1.items():
                    print(k, k1, k2, borders[k][k1][k2])
                    borders[k][k1][k2] -= placebo_value
            else:
                print(k, k1, borders[k][k1])
                borders[k][k1] -= placebo_value

    min_times = {}

    cities = []

    for tol in [2, 3, 4, 5]:
        for seconds in [60]:  # [60, 1]
            for filename in sorted(os.listdir(main_dir)):
                if not filename.startswith('duration') or filename.endswith('Dependent_any_False.pickle') or ('IndegoFlex' in filename) or (not filename.endswith('_any_False.pickle')):
                    #  or (not any([('_{}_'.format(x) in filename) for x in ['WashingtonClassic']]))
                    continue
                print(filename)
                file_data = filename.split('_')
                month_name = file_data[0].replace('duration', '')
                border_value = borders[file_data[1]][file_data[2]]
                if type(border_value) != int:
                    border_value = border_value[month_name]

                try:
                    max_time = max_times[file_data[1]][file_data[2]]
                except KeyError:
                    max_time = default_max_time
                if type(max_time) != int:
                    max_time = max_time[month_name]
                cities.append({
                    'month': month_name,
                    'city': file_data[1], 'split': file_data[2],
                    'border': border_value, 'max_time': max_time,
                    'seconds': seconds,
                    'tol': tol,
                    'any': '_'.join(file_data[3:]).replace('.pickle', ''),
                    'filenames': [os.path.join(main_dir, filename)],
                    'recompute': True,
                })

    if test_run:
        cities = cities[:1]

    print(cities)


    def saferound(x):
        try:
            return round(x)
        except OverflowError:
            return 0  # otherwise ks_2samp fails


    def intize(x):
        return np.array([saferound(i) for i in x])


    def distrs(f):
        return {
            'powerlaw': f.power_law,
            'lognormalpositive': f.lognormal_positive,
            # 'lognormal': f.lognormal,
            'exponential': f.exponential,
            # 'truncatedpowerlaw': f.truncated_power_law,
            'stretchedexponential': f.stretched_exponential,
        }


    parameter_count = {
        'lognormal': 2,
        'lognormalpositive': 2,
        'powerlaw': 1,
        'exponential': 1,
        'truncatedpowerlaw': 2,
        'stretchedexponential': 2,
    }

    distrs_underscore = {
        'lognormal': 'lognormal',
        'lognormalpositive': 'lognormal_positive',
        'powerlaw': 'power_law',
        'exponential': 'exponential',
        'truncatedpowerlaw': 'truncated_power_law',
        'stretchedexponential': 'stretched_exponential',
    }



    def functions_values(fit, distr_name, x):
        if distr_name == 'lognormalpositive':
            lower_limit = fit.lognormal_positive.xmin
            upper_limit = fit.lognormal_positive.xmax
            allxs = np.arange(lower_limit, upper_limit+1)
            scaling = fit.lognormal_positive._pdf_base_function(allxs).sum()
            return fit.lognormal_positive._pdf_base_function(x) / scaling
        if distr_name == 'lognormal':
            lower_limit = fit.lognormal.xmin
            upper_limit = fit.lognormal.xmax
            allxs = np.arange(lower_limit, upper_limit+1)
            scaling = fit.lognormal._pdf_base_function(allxs).sum()
            return fit.lognormal._pdf_base_function(x) / scaling
        elif distr_name == 'powerlaw':
            return fit.power_law._pdf_base_function(x) * fit.power_law._pdf_discrete_normalizer
        elif distr_name == 'exponential':
            return fit.exponential._pdf_base_function(x) * fit.exponential._pdf_discrete_normalizer
        elif distr_name == 'truncatedpowerlaw':
            return fit.truncated_power_law._pdf_base_function(x) * fit.truncated_power_law._pdf_discrete_normalizer
        elif distr_name == 'stretchedexponential':
            minmaxrange = np.arange(fit.stretched_exponential.xmin, fit.stretched_exponential.xmax+1)
            scaling = np.array([fit.stretched_exponential._pdf_base_function(i) for i in minmaxrange]).sum()
            stretchedexp_value = fit.stretched_exponential._pdf_base_function(x) / scaling
            return stretchedexp_value

    parts = ['left', 'right']
    additional_parts = ['border', 'total']

    def safefirst(x):
        if type(x) == str:
            return x
        else:
            return x[0]

    for city in cities:
        for shorten in {3: [0], 4: [-1, 0], 5: [0]}[city.get('tol', default_tol)]:
            print(city, shorten, datetime.now())
            city_split_str = city['split']
            c = City(city=city['city'], split=city['split'], border=city['border'],
                     single_any=city['any'],
                     min_time=city.get('min_time', default_min_time),
                     max_time=city.get('max_time', default_max_time), shorten=shorten,
                     tol=city.get('tol', default_tol), recompute=city.get('recompute', default_recompute),
                     seconds=city.get('seconds', default_seconds), month=city['month'], filenames=city['filenames'])
            if c.check_if_file_exists():
                continue

            c.load_files()
            c.seconds_transform()
            c.create_xs()
            c.dr_transform()
            c.count()

            for part in additional_parts:
                c.save_main(part)

            for part in parts:
                try:
                    c.fit_single_part(part)
                    c.predict(part)
                    c.save_main(part)
                except ValueError:
                    print('impossible to compute')

            c.save_data_res()
