import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from ast import literal_eval
import os

import seaborn as sns
sns.set_style('whitegrid')
sns.set_context('poster', rc={"lines.linewidth": 2.5})

distrs_underscore = {
    'lognormal': 'lognormal_full',
    'lognormalpositive': 'lognormal',
    'powerlaw': 'power_law',
    'exponential': 'exponential',
    'truncatedpowerlaw': 'truncated_power_law',
    'stretchedexponential': 'stretched_exp.',
}

results_path = os.path.join('..', 'out', 'finalchart_discreter.csv')

res = pd.read_csv(results_path)

for col in res.columns:
    if (col.endswith('_pvalue') or col.endswith('_stat')) and ('bestfit_' not in col):
        res[col] = res[col].astype('float64')

parts = ['left', 'right']
additional_parts = ['border']
identifier_cols = ['city', 'split', 'tol', 'seconds', 'month']
anys_col = 'single_any'
group_cols = identifier_cols + [anys_col]

colors = {
    'left': '#3dbdbe',
    'border': '#3c3950',
    'right': '#c24a65',
    'full': '#3c3950',

    'any_False': '#3dbdbe',
    'any_user_id': '#3c3950',
    'any_user_id_bearing60': '#c24a65',
    'any_bike_diff1800': '#3c3950',
    'any_bike_diff1800_bearing60': '#c24a65'
}

defaults = {'anys': ['any_False'], 'tol': 2, 'seconds': 60}

stat_significance = 0.05
ms = 6
create_new_charts_if_exists = True

markers = {'any_False': 'o',
           'any_user_id': 'D',
           'any_user_id_bearing60': 'D',
           'any_bike_diff1800': '^',
           'any_bike_diff1800_bearing60': '^'
           }
default_marker = 'o'

labels = {
    'any_False': 'all trips',
    'any_user_id': 'user with same attributes trip chaining',
    'any_user_id_bearing30': 'user with same attributes trip chaining\nbearing difference -30 to 30 degrees',
    'any_user_id_bearing45': 'user with same attributes trip chaining\nbearing difference -45 to 45 degrees',
    'any_user_id_bearing60': 'user with same attributes trip chaining\nbearing difference -60 to 60 degrees',
    'any_bike_diff120': 'same bicycle re-rented within 2 minutes',
    'any_bike_diff600': 'same bicycle re-rented within 5 minutes',
    'any_bike_diff1800': 'same bicycle re-rented within 30 minutes',
    'any_bike_diff120_bearing30': 'same bicycle re-rented within 2 minutes\nbearing difference -30 to 30 degrees',
    'any_bike_diff600_bearing30': 'same bicycle re-rented within 5 minutes\nbearing difference -30 to 30 degrees',
    'any_bike_diff1800_bearing30': 'same bicycle re-rented within 30 minutes\nbearing difference -30 to 30 degrees',
    'any_bike_diff120_bearing45': 'same bicycle re-rented within 2 minutes\nbearing difference -45 to 45 degrees',
    'any_bike_diff600_bearing45': 'same bicycle re-rented within 5 minutes\nbearing difference -45 to 45 degrees',
    'any_bike_diff1800_bearing45': 'same bicycle re-rented within 30 minutes\nbearing difference -45 to 45 degrees',
    'any_bike_diff120_bearing60': 'same bicycle re-rented within 2 minutes\nbearing difference -60 to 60 degrees',
    'any_bike_diff600_bearing60': 'same bicycle re-rented within 5 minutes\nbearing difference -60 to 60 degrees',
    'any_bike_diff1800_bearing60': 'same bicycle re-rented within 30 minutes\nbearing difference -60 to 60 degrees',
    'any_bike_stations_times2': 'any bicycle at a station re-rented within 2 minutes',
    'any_bike_stations_times5': 'any bicycle at a station re-rented within 5 minutes',
    'any_bike_stations_times30': 'any bicycle at a station re-rented within 30 minutes',
}

def safenan(row, key, return_if_nan=0, return_if_keyerror=0):
    try:
        x = row[key]
        if np.isnan(x):
            return return_if_nan
        else:
            return x
    except KeyError:
        return return_if_keyerror


def get_xs(prefix='xs',
    month='full', city='NewYork', split='Subscriber', single_any='any_False', tol=2,
    seconds=60, part='total'):
    return np.loadtxt('../out/cls/{}_{}_{}_{}_{}_{}_{}_{}.txt'.format(
        prefix, month, city, split, single_any, tol, seconds, part))


def get_preds(month='full', city='NewYork', split='Subscriber', single_any='any_False', tol=2,
    seconds=60, part='total', fit='powerlaw'):
    return np.loadtxt('../out/cls/clpred_{}_{}_{}_{}_{}_{}_{}_{}.txt'.format(
        month, city, split, single_any, tol, seconds, part, fit))


def create_chart(df, testname='pythonks', create_new_charts_if_exists=False, distrs=False):
    data_res = df.iloc[0]

    dir_name = '../charts_clean'
    os.makedirs(dir_name, exist_ok=True)
    if df.shape[0] > 1:
        dir_name = '../charts_clean_multi'
        os.makedirs(dir_name, exist_ok=True)
    elif data_res['single_any'] != defaults['anys'][0]:
        dir_name = '../charts_clean_any'
        os.makedirs(dir_name, exist_ok=True)

    final_identifier = '_'.join([str(x) for x in df.iloc[0][identifier_cols].tolist()]) + 'xx'.join(df[anys_col].tolist()) + 'dd'.join(distrs.values()) if distrs else ''
    png_name = '{}/{}.png'.format(dir_name, final_identifier)
    if (not create_new_charts_if_exists) and os.path.exists(png_name):
        return
    fig, ax = plt.subplots(figsize=(12, 4 + 3*df.shape[0]))
    bestfits = {}
    for i_row, row in df.iterrows():
        identifier = {col: row[col] for col in group_cols}
        single_any = row['single_any']

        for part in additional_parts:
            ax.plot(get_xs(prefix='xs', **identifier, part=part),
                get_xs(prefix='cl', **identifier, part=part), color=colors[single_any], linestyle='',
                    marker=markers.get(single_any, default_marker), markersize=ms, label=labels[single_any])
            
        for part in parts:
            if not distrs:
                bestfitdistr_name = row['bestfit_{}_{}'.format(testname, part)]
            else:
                bestfitdistr_name = distrs[part]
            bestfits[part] = distrs_underscore[bestfitdistr_name]
            xs = get_xs(prefix='xs', **identifier, part=part)
            xs_full = get_xs(prefix='xspred', **identifier, part=part)
            xs_border = get_xs(prefix='xs', **identifier, part='border')
            ps = get_preds(**identifier, part=part, fit=bestfitdistr_name)
            reals = get_xs(prefix='cl', **identifier, part=part)
            # if (part == 'left'):
            #     ps_sh = ps[:len(xs)]
            #     xs_full = np.hstack([xs, xs_border[:row['tol']+1]])
            # if (part == 'right'):
            #     ps_sh = ps[(row['tol'] + 1):(len(ps) - (row['tol']))]
            #     ps = ps[:(len(ps) - (row['tol']))]
            #     xs_full = np.hstack([xs_border[row['tol']:], xs])

            if part == 'left':
                ax.set_xlim(left=xs.min())
            elif part == 'right':
                ax.set_xlim(right=xs.max())

            print(xs_full, ps)
            if len(xs_full) == len(ps):
                ax.plot(xs_full, ps,
                    color=colors[single_any],
                    linestyle='solid' if safenan(row, '{}_fit_{}_{}_pvalue'.format(
                        part, bestfitdistr_name, testname)) > stat_significance else 'dotted')
            else:
                print('at least one prediction empty', identifier, len(xs), len(ps))

            ax.plot(xs, reals, color=colors[single_any], linestyle='',
                    marker=markers.get(single_any, default_marker), markersize=ms)

            ax.axvline(x=data_res['border']*(60 / data_res['seconds']), c=colors['full'], lw=1)

    if df.shape[0] == 1:
        ax.set_title(data_res['city'] + ' – ' + data_res['split'] + '\nRemove: ' + labels[data_res['single_any']], fontsize=24)
        ax.text(get_xs(prefix='xs', **identifier, part='left').min() + 1, get_xs(prefix='cl', **identifier, part='right').min() * 1.5,
            'left: ' + bestfits['left'], fontsize=20)
        ax.text(get_xs(prefix='xs', **identifier, part='right').min() + 1, get_xs(prefix='cl', **identifier, part='left').max() * 0.5,
            'right: ' + bestfits['right'], fontsize=20)
    else:
        ax.set_title(data_res['city'] + ' – ' + data_res['split'], fontsize=24)
        fig.legend(loc='lower left', bbox_to_anchor=(0.1, 0.1))


    ax.set_xlabel('Duration [minutes]', loc='left')
    ax.set_ylabel('Number of trips', loc='bottom')
    
    ax.set_xscale('log')
    ax.set_yscale('log')

    fig.tight_layout()
    fig.savefig(png_name)
    # plt.show()
    plt.close(fig)


data_final = []
multiple_charts = []

for tol in [4]:
    for anys in [
        ['any_False', 'any_user_id', 'any_user_id_bearing60'],
        ['any_False', 'any_bike_diff1800', 'any_bike_diff1800_bearing60'],
    ]:
        multiple_charts.extend([{'city': 'Chicago', 'month': month, 'split': ['Subscriber', 'member'], 'anys': anys, 'tol': tol} for month in ['19x07', '19x11']])
        multiple_charts.extend([{'city': 'NYC', 'month': month, 'split': ['Subscriber', 'member'], 'anys': anys, 'tol': tol} for month in ['19x07', '19x11', '20x07', '20x11']])

    multiple_charts.extend([{'city': 'Chicago', 'month': month, 'split': ['Subscriber', 'member'], 'anys': ['any_False', 'any_bike_diff1800', 'any_bike_diff1800_bearing60'], 'tol': tol} for month in ['20x07', '20x11']])
    multiple_charts.extend([{'city': 'Washington', 'month': month, 'split': ['Member', 'member'], 'anys': ['any_False', 'any_bike_diff1800', 'any_bike_diff1800_bearing60'], 'tol': tol} for month in ['19x07', '19x11', '20x07', '20x11']])
    multiple_charts.extend([{'city': 'Warsaw', 'month': month, 'split': ['all'], 'anys': ['any_False', 'any_bike_diff1800', 'any_bike_diff1800_bearing60'], 'tol': tol} for month in ['19x11']])

for chart_data in multiple_charts:
    print(chart_data)
    res_sh = res.loc[(res['city'] == chart_data['city']) & (res['month'] == chart_data['month']) ]
    if splits := chart_data.get('split'):
        res_sh = res_sh.loc[res['split'].isin(splits)]
    if anys := chart_data.get('anys', [defaults['anys']]):
        res_sh = res_sh.loc[res_sh['single_any'].isin(anys)]

    for v in ['seconds', 'tol']:
        if value := chart_data.get(v, defaults[v]):
            res_sh = res_sh.loc[res_sh[v] == value]
    create_chart(res_sh, create_new_charts_if_exists=True, testname='pythonks',
        distrs={'left': 'lognormalpositive', 'right': 'powerlaw'})
    create_chart(res_sh, create_new_charts_if_exists=True, testname='pythonks',
        distrs={'left': 'stretchedexponential', 'right': 'powerlaw'})
